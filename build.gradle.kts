plugins {
    kotlin("jvm") version "1.9.0"
    java
    id("org.jlleitschuh.gradle.ktlint") version "11.6.1"
}

group = "de.thecastor"
version = "0.0.1"

apply(from = "versions.gradle")

repositories {
    mavenCentral()
}

val jUnitVersion: String by extra

dependencies {
    implementation(kotlin("reflect"))

    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:$jUnitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$jUnitVersion")
}

kotlin {
    jvmToolchain(17)
}

tasks {
    test {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }
}

ktlint {
    version.set("1.0.1")
}
